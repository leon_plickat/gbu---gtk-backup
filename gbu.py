#!/usr/bin/python3
import gi			# For the GUI
import os			# To use the file system
import getpass		# To get the username
import time
import subprocess
import socket		# To get the hostname
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio


UIfile = "/usr/share/gbu/window.ui"
Hostname = socket.gethostname()
DriveLocation = "/media/" + getpass.getuser() + "/BU_Drive"


BackupCommand = ["pkexec", "rsync", "-aHv", "--delete", "--progress", "/etc", "/home", DriveLocation + "/BU_Backups/" + Hostname + "/"]
RestoreCommand = ["pkexec", "rsync", "-aHv", "--delete", "--progress", DriveLocation + "/BU_Backups/" + Hostname + "/home/", "/home/"]


def GetDriveStatus():
	return(os.path.isdir(DriveLocation))

class TOOL(Gtk.Application):
	
	def __init__(self):
		Gtk.Application.__init__(self, application_id='org.smurphy.gbu', flags=Gio.ApplicationFlags.FLAGS_NONE)
		self.connect("activate", self.OnActivate)
		
	def OnActivate(self, data=None):
		ActiveWindows = self.get_windows()
		if len(ActiveWindows) > 0:
			# Focus window if aleardy running
			self.get_active_window().present()
		else:
			self.CreateMainWindow()
			
			
	def CreateMainWindow(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file(UIfile)
		self.window = self.builder.get_object("window1")
		
		self.window.set_icon_name("gbu")
		
		self.window.show_all()
		self.add_window(self.window)
		
		self.builder.get_object("ButtonBackup").connect("clicked", self.backup)
		self.builder.get_object("ButtonRestore").connect("clicked", self.restore)
		self.builder.get_object("ButtonRefresh").connect("clicked", self.update)
		self.builder.get_object("ButtonAbout").connect("clicked", self.about)
		
		# Set label to show if drive is found
		#Note "none" when useing the self.update method is only there to
		# prevent an typerror
		self.update("none")
		
		
		# Currently using zenity to provide dialogs
		# Will make own in the future
		
	def backup(self, button):
		if self.update("none"):
			try:
				os.mkdir(DriveLocation + "/BU_Backups/")
				os.mkdir(DriveLocation + "/BU_Backups/" + Hostname)
			finally:
				time.sleep(1)
				ExitCode = subprocess.Popen(BackupCommand).wait()
				print(ExitCode)
				if ExitCode == 0:
					subprocess.Popen(["zenity", "--info", "--text=Backup Completed"])
				elif ExitCode != 126:
					subprocess.Popen(["zenity", "--error", "--text=An error occured during the backup"])
				self.update("none")
		
	def restore(self, button):
		if self.update("none"):
			ContinueRestore = subprocess.Popen(["zenity", "--question", "--text=Warning: Any files created after the last backup will be deleted. Do you wish to contiune?"]).wait()
			if ContinueRestore != 0:
				print("Stoping restore")
				return 0
			print(ContinueRestore)
			ExitCode = subprocess.Popen(RestoreCommand).wait()
			print(ExitCode)
			if ExitCode == 0:
				subprocess.Popen(["zenity", "--info", "--text=Restoration Completed"])
			elif ExitCode != 126:
				subprocess.Popen(["zenity", "--error", "--text=An error occured while restoring"])
				self.update("none")
			
	def update(self, button):
		if GetDriveStatus():
			self.builder.get_object("LabelDriveStatus").set_text("Backup Drive is ready!")
			self.builder.get_object("ButtonBackup").set_sensitive(1)
			self.builder.get_object("ButtonRestore").set_sensitive(1)
			return(True)
		else:
			self.builder.get_object("LabelDriveStatus").set_text("Backup Drive is not found!")
			self.builder.get_object("ButtonBackup").set_sensitive(0)
			self.builder.get_object("ButtonRestore").set_sensitive(0)
			return(False)
			
	def about(self, button):
		self.builder = Gtk.Builder()
		self.builder.add_from_file(UIfile)
		self.AboutWindow = self.builder.get_object("WindowAbout")
		self.AboutWindow.show_all()
		self.add_window(self.AboutWindow)
	



if __name__ == "__main__":
    app = TOOL()
    app.run(None)
